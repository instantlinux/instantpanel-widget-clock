export default function createWidget(widgetfolder: string, isvertical: boolean) {
    // Main widget container
    let clockcontainer = document.createElement("div");
    // Style
    clockcontainer.style.position  = "absolute";
    clockcontainer.style.top       = "0";
    clockcontainer.style.left      = "0";
    clockcontainer.style.right     = "0";
    clockcontainer.style.bottom    = "0";

    // P that displays the current date and time
    let clocktext = document.createElement("p");
    // Style
    clocktext.style.fontFamily    = "Raleway";
    clocktext.style.textAlign     = "center";
    clocktext.style.lineHeight    = "1.5";
    clocktext.style.fontSize      = "14px";
    clocktext.style.color         = "#fafafa";
    clocktext.style.letterSpacing = "0.05em";
    clocktext.style.fontWeight    = "600";
    clocktext.style.margin        = "0";
    clocktext.style.position      = "absolute";
    clocktext.style.top           = "50%";
    clocktext.style.left          = "0";
    clocktext.style.right         = "0";
    clocktext.style.transform     = "translate(0, -50%)";


    // Adds a zero before a number, if the number is less than 10.
    var addZeros = function(i: number) {
        let returnstring: string = i.toString();
        if (i < 10) {
            returnstring = "0" + i;
        }
        return returnstring;
    }

    var weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    // Refreshes time and date.
    var refreshclock = function() {
        console.log("Refreshing clock...")
        // Get the current time
        let currenttime = new Date();
        // Set text.
        clocktext.innerHTML = currenttime.getHours() + ":" + addZeros(currenttime.getMinutes());
        let datetext = document.createElement("span");
        datetext.style.fontWeight = "300";

        if (!isvertical) {
            datetext.innerHTML = "" + weekdays[currenttime.getDay()] + ", " + addZeros(currenttime.getDate()) + "." + addZeros(currenttime.getMonth() + 1);
            datetext.style.paddingLeft = "20px";
        } else {
            datetext.innerHTML = "<br>" + weekdays[currenttime.getDay()] + ", " + addZeros(currenttime.getDate()) + "." + addZeros(currenttime.getMonth() + 1);
        }
        clocktext.appendChild(datetext);

        // Refresh the clock after 10 seconds.
        setTimeout(refreshclock, 10000);
    }

    // Initialize the clock
    refreshclock();

    // Add timetext and datetext to the main container
    clockcontainer.appendChild(clocktext);

    return clockcontainer;
}